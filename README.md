# Quiz Game
Quiz Game is a web application where it is possible to create and play quizzes.
The application was written in Kotlin. The backend is using Spring Boot.

https://vargajana95-quiz-game.herokuapp.com/

Features:
* Create new quizzes
* Edit the quizzes
* Export/Import quizzes
* Play a selected quiz real time, where others have to connect and answer the questions

![](images/Capture1.png)

![](images/Capture2.png)

![](images/Capture5.png)

![](images/Capture3.png)

![](images/Capture4.png)



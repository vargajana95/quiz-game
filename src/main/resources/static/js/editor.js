let token = document.querySelector("meta[name='_csrf']").getAttribute("content");
let csrfHeader = document.querySelector("meta[name='_csrf_header']").getAttribute("content");
let quizUUID = document.querySelector("#questionList").getAttribute("data-quiz-uuid");
let quizId = document.querySelector("#questionList").getAttribute("data-quiz-id");


function Choice(placeholder) {
    const self = this;
    self.choice = ko.observable();
    self.correct = ko.observable(false);
    self.placeholder = ko.observable(placeholder);

}

function Question() {
    const self = this;
    self.question = ko.observable();
    self.imageUrl = ko.observable();
    self.imageId = ko.observable();
    self.choices = ko.observableArray();
}

function Quiz() {
    const self = this;

    self.imageUrl = ko.observable();
    self.imageId = ko.observable();
    self.title = ko.observable();
    self.questions = ko.observableArray();
}

function ViewModel() {
    const self = this;

    let q = new Quiz();
    q.questions.push(createNewQuestion());
    self.quiz = ko.observable(q);

    function createNewQuestion() {
        let question = new Question();
        question.choices([new Choice("A"), new Choice("B"), new Choice("C"), new Choice("D")]);
        return question;
    }

    self.deleteQuestion = function(question) {
        const index = self.quiz().questions.indexOf(question);
        if (index > -1) {
            self.quiz().questions.splice(index, 1);
        }
    };

    self.addQuestion = function () {
        self.quiz().questions.push(createNewQuestion());
        $("html, body").animate({
            scrollTop: document.body.scrollHeight
        }, 1000);
    };

    self.save = function () {

        fetch('/quizzes/edit', {
            method: 'PUT',
            headers: {
                [csrfHeader]: token,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({...{id: quizUUID}, ...ko.mapping.toJS(self.quiz, {'ignore': ["placeholder", "imageUrl"]})},)
        })
            .then(response => {
                if (response.ok) {
                    toastr.success('Changes were successfully saved!');

                } else {
                    toastr.error('Ooops! Something went wrong. Please try again!')
                }
            });
    };

    self.fileUpload = function (data, event) {
        const formData = new FormData();
        formData.append("image", event.target.files[0]);
        fetch('/images/upload', {
            method: 'POST',
            headers: {
                [csrfHeader]: token,
            },
            body: formData
        }).then(response => {
                if (response.ok) {
                    response.json().then(responseBody => {
                        data.imageId(responseBody.id);
                        data.imageUrl(responseBody.url);
                    });
                } else {
                    alert("error");
                }
            }
        );

    };

    function getQuizData() {
        fetch('/rest/quizzes/' + quizId)
            .then(response => {
                if (response.ok) {
                    response.json().then(quiz => {
                        console.log(quiz);

                        let newQuiz = new Quiz();
                        newQuiz.title(quiz.title);
                        newQuiz.imageId(quiz.imageId);
                        newQuiz.imageUrl(quiz.imageUrl);
                        newQuiz.questions(quiz.questions.map(q => {
                            let question = new Question();
                            question.question(q.question);
                            question.imageId(q.imageId);
                            question.imageUrl(q.imageUrl);
                            question.choices(q.choices.map(c => {
                                let choice = new Choice("");
                                choice.choice(c.choice);
                                choice.correct(c.correct);
                                return choice;
                            }));
                            return question;
                        }));
                        self.quiz(newQuiz);

                    })
                }
            })
    }

    getQuizData();


}

ko.applyBindings(new ViewModel());

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const quizId = urlParams.get("quizId");
const playIdText = $("#playId");
let stompClient = null;

let token = $("meta[name='_csrf']").attr("content");
let header = $("meta[name='_csrf_header']").attr("content");

let gameId = null;


function Player() {
    const self = this;

    self.name = ko.observable();
}

function Choice() {
    const self = this;

    self.letter = ko.observable();
    self.choice = ko.observable();
}

function Question() {
    const self = this;

    self.question = ko.observable();
    self.choices = ko.observableArray();
    self.imageUrl = ko.observable();
}

function ViewModel() {
    const self = this;

    let quiz = null;


    self.players = ko.observableArray();
    self.showQuestion = ko.observable(false);
    self.showLobby = ko.observable(true);
    self.showResults = ko.observable(false);
    self.gameEnded = ko.observable(false);
    self.currentQuestion = ko.observable();


    self.requestStartGame = function (data) {
        if (gameId) {
            stompClient.send("/app/" + gameId + "/start", {});
        }
    };


    function startGame(data) {
        console.log(JSON.parse(data.body));
        self.requestNextQuestion();
        self.showLobby(false);
        self.showQuestion(true);
    }

    self.requestNextQuestion = function () {
        if (gameId) {
            stompClient.send("/app/" + gameId + "/next", {});
        }
    };

    self.endQuestion = function () {
        if (gameId) {
            stompClient.send("/app/" + gameId + "/endQuestion", {});
        }
    };

    function nextQuestion(data) {
        console.log(JSON.parse(data.body));
        const receivedData = JSON.parse(data.body);
        const questionData = quiz.questions[receivedData.questionNumber];
        let question = new Question();
        question.question(questionData.question);
        question.imageUrl(questionData.imageUrl);
        const letters = ["A", "B", "C", "D"];
        questionData.choices.forEach((it, index) => {
            let choice = new Choice();
            choice.choice(it.choice);
            choice.letter(letters[index]);
            question.choices.push(choice);
        });

        self.currentQuestion(question);
        self.showQuestion(true);
    }

    function endQuiz(data) {
        // const results = JSON.parse(data.body);
        // choicesList.empty();
        // const resultList = $("#resultList");
        // results.forEach ((it) => {
        //     const li = $("<li></li>");
        //     li.text(it.name + ": " + it.score);
        //     resultList.append(li);
        // })
    }


    function createGame() {
        let headers = {};
        headers[header] = token;
        $.ajax("/rest/createGame/" + quizId, {
            type: "POST",
            dataType: "json",
            headers: headers
        })
            .done(function (data) {
                console.log(data);

                gameId = data.gameId;
                quiz = data.quiz;
                playIdText.text(data.gameId);
                connect()
            })

            .fail(function () {
                alert("error");
            });
    }

    createGame();


    function connect() {
        const socket = new SockJS('/ws');
        stompClient = Stomp.over(socket);
        stompClient.connect('', '', function (frame) {
            let whoami = frame.headers['user-name'];
            console.log('Connected: ' + frame);
            console.log('Whoami: ' + whoami);


            stompClient.subscribe('/user/queue/' + gameId + "/join", function (data) {
                let player = JSON.parse(data.body);
                console.log(player.name);
                self.players.push(player);
            });


            stompClient.subscribe('/user/queue/' + gameId + "/start", startGame);
            stompClient.subscribe('/user/queue/' + gameId + "/next", nextQuestion);
            stompClient.subscribe('/user/queue/' + gameId + "/endQuestion", receiveEndQuestion);

            stompClient.subscribe('/user/queue/' + gameId + "/end", data => {
                self.gameEnded(true);
                receiveEndQuestion(data);
            });

        });
    }

    function receiveEndQuestion(data) {
        const results = JSON.parse(data.body);
        results.sort((a, b) => a.score - b.score);
        self.players.splice(0, self.players().length);

        for (let i = 0; i < Math.min(3, results.length); i++) {
            self.players.push({name: results[i].name, score: results[i].score});
        }

        self.showResults(true);
        self.showQuestion(false);
    }

}

ko.applyBindings(new ViewModel());


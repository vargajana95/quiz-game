let stompClient = null;
let gameIdInput = $("#gameId");
let gameIdForm = $("#gameIdForm");
let nameInput = $("#nameInput");

let gameId = null;
function ViewModel() {
    const self = this;

    self.isInLobby = ko.observable(false);
    self.isJoining = ko.observable(true);
    self.showChoices = ko.observable(false);
    self.showResult = ko.observable(false);
    self.choiceSubmitted = ko.observable(false);
    self.correctChoice = ko.observable(false);

    self.choices = ko.observableArray();

    self.join = function () {
        const socket = new SockJS('/ws');
        stompClient = Stomp.over(socket);
        stompClient.connect('', '', function(frame) {
            console.log('Connected: ' + frame);

            gameId = gameIdInput.val();
            const name =  nameInput.val();

            stompClient.subscribe('/user/queue/' + gameId+"/join", function (data) {
                let joinData = JSON.parse(data.body);
                console.log(joinData);
                self.isInLobby(true);
                self.isJoining(false);
            });

            stompClient.subscribe('/user/queue/' + gameId+"/start", function (data) {
                let joinData = JSON.parse(data.body);
                console.log(joinData);
                self.isInLobby(false);
                self.showChoices(true);
            });

            stompClient.subscribe('/user/queue/' + gameId+"/next", function (data) {
                let joinData = JSON.parse(data.body);
                console.log(joinData);
                let letters = ["A", "B", "C", "D"];
                    self.choices.splice(0,self.choices().length);
                letters.forEach((letter, index) => {
                    self.choices.push({letter: letter, index: index});
                });

                self.showChoices(true);
                self.showResult(false);
            });

            stompClient.subscribe('/user/queue/' + gameId+"/endQuestion", function (data) {
                let parsed = JSON.parse(data.body);

                self.correctChoice(parsed.correct);
                self.showResult(true);
                self.choiceSubmitted(false);
                self.showChoices(false);
            });

            stompClient.subscribe('/user/queue/' + gameId+"/end", function (data) {
                let parsed = JSON.parse(data.body);

                self.correctChoice(parsed.correct);
                self.showResult(true);
                self.choiceSubmitted(false);
                self.showChoices(false);
            });

            stompClient.send("/app/"+gameId+"/join", {}, JSON.stringify({name: name}));
        });
    };


    self.sendChoice = function (target) {

        self.choiceSubmitted(true);
        self.showChoices(false);
        stompClient.send("/app/"+gameId+"/choice", {}, JSON.stringify({choice: target.index}));

    };
}


ko.applyBindings(new ViewModel());


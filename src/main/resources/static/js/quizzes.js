let token = document.querySelector("meta[name='_csrf']").getAttribute("content");
let csrfHeader = document.querySelector("meta[name='_csrf_header']").getAttribute("content");

function Quiz() {
    const self = this;
    self.title = ko.observable();
    self.id = ko.observable();

}

function ViewModel() {
    const self = this;
    self.quizzes = ko.observableArray();

    self.addQuiz = function () {
        return new Quiz();
    };

    self.deleteQuiz = function (quiz) {
        deleteQuizById(quiz.id())
            .then(response => {
                if (response.ok) {
                    self.quizzes.remove(quiz);
                    toastr.success("Successfully deleted quiz!");
                } else {
                    toastr.error("Could not delete Quiz!");
                }
            });
    }
}

async function deleteQuizById(id) {
    return fetch('/quizzes/' + id, {
        method: 'DELETE',
        headers: {
            [csrfHeader]: token,
            'Content-Type': 'application/json'
        }
    });
}

ko.applyBindings(new ViewModel());

document.getElementById("import-button").onclick = function () {
    const formData = new FormData();
    formData.append("quizDataFile", document.getElementById("import-quiz-file").files[0]);
    fetch('/quizzes/import', {
        method: 'POST',
        headers: {
            [csrfHeader]: token,
        },
        body: formData
    }).then(response => {
            if (response.ok) {
                //toastr.success("Successfully imported the Quiz!");
                //$('#importModal').modal('hide');
                var searchParams = new URLSearchParams(window.location.search);
                searchParams.set("importSuccess", "true");
                window.location.search = searchParams.toString();
            } else {
                toastr.error("An error has occurred! Please try again!")
            }
        }
    );
};

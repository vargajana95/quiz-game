package vargajana95.quizgame.exception


class EntityNotFoundException(message: String) : Exception(message)

package vargajana95.quizgame.exception

import java.lang.Exception

class GameNotExistsException(message: String) : Exception(message)

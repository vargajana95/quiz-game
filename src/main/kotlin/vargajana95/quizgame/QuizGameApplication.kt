package vargajana95.quizgame

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaAuditing

@SpringBootApplication
class QuizGameApplication

fun main(args: Array<String>) {
    runApplication<QuizGameApplication>(*args)
}

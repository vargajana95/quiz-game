package vargajana95.quizgame.controller

import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import vargajana95.quizgame.dto.RegisterDto
import vargajana95.quizgame.service.UserService
import javax.validation.Valid

@Controller
class UserController(private val userService: UserService) {

    @GetMapping("register")
    fun showRegistration(@ModelAttribute("user") registration: RegisterDto) = "registration"

    @PostMapping("/register")
    fun createUser(@Valid @ModelAttribute("user") registration: RegisterDto, result: BindingResult): String {
        if (result.hasErrors()) {
            return "registration"
        }

        userService.createUser(registration)

        return "redirect:/login?registration"
    }
}
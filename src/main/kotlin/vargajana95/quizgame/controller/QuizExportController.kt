package vargajana95.quizgame.controller

import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.ResponseBody
import vargajana95.quizgame.domain.AppUserDetails
import vargajana95.quizgame.service.QuizExportService

@Controller
class QuizExportController(
        private val quizExportService: QuizExportService
) {

    @GetMapping(path = ["/quizzes/{quizId}/export"])
    @ResponseBody fun  exportSingleQuiz(@PathVariable quizId: Long): ResponseEntity<ByteArray> {
        return ResponseEntity.ok().header("Content-Disposition","attachment; filename=QuizExport.json").body(quizExportService.exportSingleQuiz(quizId))
    }

    @GetMapping(path = ["/quizzes/export"])
    @ResponseBody fun  exportMyQuizzes(@AuthenticationPrincipal user: AppUserDetails): ResponseEntity<ByteArray> {
        return ResponseEntity.ok().header("Content-Disposition","attachment; filename=QuizExport.json").body(quizExportService.exportQuizzesForUser(user.id!!))
    }
}
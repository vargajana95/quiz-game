package vargajana95.quizgame.controller

import mu.KotlinLogging
import org.springframework.messaging.MessageHeaders
import org.springframework.messaging.handler.annotation.DestinationVariable
import org.springframework.messaging.handler.annotation.Header
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.messaging.simp.SimpMessageHeaderAccessor
import org.springframework.messaging.simp.SimpMessageType
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import vargajana95.quizgame.domain.GameData
import vargajana95.quizgame.dto.JoinRequest
import vargajana95.quizgame.dto.JoinResponse
import vargajana95.quizgame.dto.QuizResponseDto
import vargajana95.quizgame.exception.GameNotExistsException
import vargajana95.quizgame.service.QuizService
import java.security.Principal
import kotlin.math.pow
import kotlin.random.Random


private val logger = KotlinLogging.logger {}

@Controller
class PlayController(
        private val quizService: QuizService,
        private val messagingTemplate: SimpMessagingTemplate
) {
    private val plays: MutableMap<String, GameData> = hashMapOf()

    @GetMapping("/quizzes/play")
    fun showCreateGame(@RequestParam("quizId") quizId: Long, model: Model): String {
        return "createGame"
    }

    @GetMapping("/play")
    fun showPlay(): String {
        return "play"
    }


    @PostMapping("/rest/createGame/{quizId}")
    @ResponseBody
    fun createGame(@PathVariable quizId: Long, principal: Principal): Map<String, Any> {
        val gameId = generateRandomDigits(7)

        val quiz = quizService.getQuizById(quizId)
        val quizData = QuizResponseDto(quiz)
        plays.putIfAbsent(gameId, GameData(gameId, principal.name, quiz.questions.size, quizData))

        logger.debug { "${principal.name} created a game. PlayId: $gameId" }

        return mapOf("gameId" to gameId, "quiz" to quizData)
    }

    fun generateRandomDigits(n: Int): String {
        val m = 10.0.pow((n - 1).toDouble()).toInt()
        return (m + Random.nextInt(9 * m)).toString()
    }


    @MessageMapping("/{gameId}/join")
    fun join(@DestinationVariable gameId: String, @Payload joinRequest: JoinRequest, @Header(name = "simpSessionId") sessionId: String) {

        val gameData = getGameData(gameId)

        logger.debug { "$sessionId joined the game: $gameId" }

        gameData.createPlayer(sessionId, joinRequest.name)

        messagingTemplate.convertAndSendToUser(gameData.creator, "/queue/$gameId/join", JoinResponse(joinRequest.name))

        sendToUser(sessionId, "/queue/$gameId/join", mapOf("success" to true))
    }

    @MessageMapping("/{gameId}/start")
    fun start(@DestinationVariable gameId: String, principal: Principal) {
        val gameData = getGameData(gameId)

        if (gameData.creator == principal.name) {
            logger.debug { "${principal.name} started the game: $gameId" }

            gameData.players.forEach { (_, v) -> sendToUser(v.sessionId, "/queue/$gameId/start", mapOf("status" to "started")) }
            messagingTemplate.convertAndSendToUser(principal.name, "/queue/$gameId/start", mapOf("status" to "started"))

        }

    }

    @MessageMapping("/{gameId}/choice")
    fun choice(@DestinationVariable gameId: String, @Header(name = "simpSessionId") sessionId: String, choice: Map<String, Int>) {

        val gameData = getGameData(gameId)

        val choiceNum = choice["choice"]
        if (choiceNum != null) {
            gameData.players[sessionId]?.setChoice(gameData.questionNumber, choiceNum, gameData.quiz.questions[gameData.questionNumber].choices[choiceNum].correct)
        }
        val nextQuestion = gameData.players.all { (_, v) -> v.choices[gameData.questionNumber].choice != null }
        if (nextQuestion) {
            showResults(gameData)
        }
    }

    @MessageMapping("/{gameId}/next")
    fun receiveNext(@DestinationVariable gameId: String, principal: Principal) {

        val gameData = getGameData(gameId)
        if (gameData.creator == principal.name) {

            next(gameData)

        }
    }

    @MessageMapping("/{gameId}/endQuestion")
    fun receiveEndQuestion(@DestinationVariable gameId: String, principal: Principal) {

        val gameData = getGameData(gameId)
        if (gameData.creator == principal.name) {

            showResults(gameData)
        }
    }

    private fun showResults(gameData: GameData) {
        val gameId = gameData.gameId

        val endpoint = if (gameData.questionNumber < gameData.questionCount-1) "endQuestion" else "end";

        gameData.players.forEach { (_, v) ->
            val wasCorrect = gameData.players[v.sessionId]?.getChoice(gameData.questionNumber)!!.correct;
            sendToUser(v.sessionId, "/queue/$gameId/$endpoint", mapOf("correct" to wasCorrect, "score" to gameData.players[v.sessionId]?.currentScore))
        }

        val resultResponse = gameData.players.map { (_, v) -> mapOf("name" to v.name, "score" to v.currentScore) }
        messagingTemplate.convertAndSendToUser(gameData.creator, "/queue/$gameId/$endpoint", resultResponse)
    }

    private fun next(gameData: GameData) {

        val gameId = gameData.gameId
        logger.debug { "${gameData.creator}@$gameId next question" }
        gameData.questionNumber++

        if (gameData.questionNumber < gameData.questionCount) {
            gameData.players.forEach { (_, v) -> sendToUser(v.sessionId, "/queue/$gameId/next", mapOf("questionNumber" to gameData.questionNumber)) }
            messagingTemplate.convertAndSendToUser(gameData.creator, "/queue/$gameId/next", mapOf("questionNumber" to gameData.questionNumber))
        }

    }


    private fun getGameData(gameId: String) = plays[gameId] ?: throw GameNotExistsException("Game not found!")

    private fun sendToUser(sessionId: String, path: String, payload: Any) {
        messagingTemplate.convertAndSendToUser(sessionId, path, payload, getSessionHeaders(sessionId))

    }

    private fun getSessionHeaders(sessionId: String): MessageHeaders {
        val headerAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE)
        headerAccessor.sessionId = sessionId
        headerAccessor.setLeaveMutable(true)
        return headerAccessor.messageHeaders;
    }
}
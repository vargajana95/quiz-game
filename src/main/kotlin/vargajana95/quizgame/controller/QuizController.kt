package vargajana95.quizgame.controller

import org.springframework.http.MediaType
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import vargajana95.quizgame.domain.AppUserDetails
import vargajana95.quizgame.domain.GameData
import vargajana95.quizgame.domain.Quiz
import vargajana95.quizgame.dto.CreateQuizDto
import vargajana95.quizgame.dto.QuizResponseDto
import vargajana95.quizgame.service.QuizService
import java.security.Principal
import java.util.*
import javax.validation.Valid

@Controller
class QuizController(
        private val quizService: QuizService
) {

    @GetMapping("/quizzes")
    fun listQuizzes(@AuthenticationPrincipal user: AppUserDetails, model: Model): String {
        model["quizzes"] = quizService.getQuizzesForUser(user.id!!)

        return "quizzes"
    }


    @GetMapping("/quizzes/{id}/details")
    fun quizDetails(@PathVariable("id") quizId: Long, model: Model): String {
        model["quiz"] = quizService.getQuizById(quizId)

        return "quizDetails"
    }

    @GetMapping("/quizzes/{id}/edit")
    fun editQuiz(@PathVariable("id") quizId: Long, model: Model): String {
        model["quiz"] = quizService.getQuizById(quizId)
        return "quizEditor"
    }

    @GetMapping("/rest/quizzes/{quizId}")
    @ResponseBody
    fun getQuizData(@PathVariable quizId: Long): QuizResponseDto {
        val quiz = quizService.getQuizById(quizId)
        val quizData = QuizResponseDto(quiz)

        return quizData
    }

    @GetMapping("/quizzes/new")
    fun showEditor(model: Model): String {
        val quiz = quizService.createNewQuizForCurrentUser()
        model["quiz"] = quiz
        return "quizEditor"
    }

    @PutMapping("/quizzes/edit")
    @ResponseBody fun editOrCreateQuiz(@Valid @RequestBody quiz: CreateQuizDto) {
        quizService.createOrUpdateQuiz(quiz)
    }

    @DeleteMapping("/quizzes/{quizId}")
    @ResponseBody fun deleteQuiz(@PathVariable quizId: Long) {
        quizService.deleteQuiz(quizId)
    }


}
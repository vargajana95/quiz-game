package vargajana95.quizgame.controller

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.multipart.MultipartFile
import vargajana95.quizgame.dto.export.QuizExportData
import vargajana95.quizgame.service.QuizImportService

@Controller
class QuizImportController(
        private val quizImportService: QuizImportService
) {

    @PostMapping("/quizzes/import")
    @ResponseBody
    fun importQuiz(@RequestParam quizDataFile: MultipartFile) {
        val mapper = ObjectMapper()

        val quizData = mapper.readValue(quizDataFile.bytes, Array<QuizExportData>::class.java)

        quizImportService.importQuizzes(quizData.toList())
    }
}
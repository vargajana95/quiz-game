package vargajana95.quizgame.controller

import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import vargajana95.quizgame.service.ImageService

@Controller
class ImageController(
        val imageService: ImageService
) {

    @PostMapping("/images/upload")
    @ResponseBody fun uploadImage(@RequestParam image: MultipartFile): Map<String, Any?> {

        val savedImage = imageService.saveImage(image.bytes)

        return mapOf("url" to "/images/${savedImage.uuid}", "id" to savedImage.id)
    }

    @GetMapping("/images/{imageId}", produces = [MediaType.IMAGE_JPEG_VALUE])
    @ResponseBody fun getImage(@PathVariable imageId: String): ByteArray {
        return imageService.getImage(imageId).data
    }
}
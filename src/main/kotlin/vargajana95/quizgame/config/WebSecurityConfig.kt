package vargajana95.quizgame.config

import org.springframework.context.annotation.Bean
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.password.NoOpPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class WebSecurityConfig: WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity) {
        http
                .formLogin()
                .defaultSuccessUrl("/quizzes")
                .permitAll()
                .defaultSuccessUrl("/", false)
                .and()
                .logout()
                .and()
                .authorizeRequests()
                .antMatchers("/register").permitAll()
                .antMatchers("/js/**", "/media/**","/css/**").permitAll()
                .antMatchers("/play", "/ws/**").permitAll()
        .anyRequest().authenticated()
    }

    @Suppress("DEPRECATION")
    @Bean
    fun passwordEncoder(): PasswordEncoder = NoOpPasswordEncoder.getInstance()
}
package vargajana95.quizgame.config

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.messaging.MessageSecurityMetadataSourceRegistry
import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer

@Configuration
class WebSocketSecurityConfig: AbstractSecurityWebSocketMessageBrokerConfigurer() {

    @Override
    override fun configureInbound(messages: MessageSecurityMetadataSourceRegistry) {
        messages
                //.nullDestMatcher().authenticated()
        .simpSubscribeDestMatchers("/user/queue/**").permitAll()
        .simpDestMatchers("/app/**").permitAll()
        //.anyMessage().denyAll()
    }

    override fun sameOriginDisabled()= true
}
package vargajana95.quizgame.config

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.core.annotation.Order
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter


@Order(1)
@Configuration
@Profile(value = ["dev"])
class H2ConsoleConfiguration : WebSecurityConfigurerAdapter() {
    override fun configure(http: HttpSecurity) {
        val h2Console = http.antMatcher("/h2-console/**")
        h2Console.csrf().disable()
        h2Console.headers().frameOptions().sameOrigin()
    }
}
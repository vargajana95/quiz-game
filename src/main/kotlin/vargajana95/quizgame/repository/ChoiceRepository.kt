package vargajana95.quizgame.repository

import org.springframework.data.jpa.repository.JpaRepository
import vargajana95.quizgame.domain.Choice
import vargajana95.quizgame.domain.Question

interface ChoiceRepository: JpaRepository<Choice, Long> {
    fun deleteAllInBatchByQuestion(question: Question)
}
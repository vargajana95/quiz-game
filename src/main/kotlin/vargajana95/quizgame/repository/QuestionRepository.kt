package vargajana95.quizgame.repository

import org.springframework.data.jpa.repository.JpaRepository
import vargajana95.quizgame.domain.Question
import vargajana95.quizgame.domain.Quiz

interface QuestionRepository: JpaRepository<Question, Long> {
    fun deleteAllInBatchByQuiz(quiz: Quiz)
}
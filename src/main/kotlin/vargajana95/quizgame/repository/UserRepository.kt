package vargajana95.quizgame.repository

import org.springframework.data.jpa.repository.JpaRepository
import vargajana95.quizgame.domain.User

interface UserRepository: JpaRepository<User, Long> {
    fun findByEmail(email: String): User?
    fun existsByEmail(email: String): Boolean
}
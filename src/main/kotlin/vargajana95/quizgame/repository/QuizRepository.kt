package vargajana95.quizgame.repository

import org.springframework.data.jpa.repository.JpaRepository
import vargajana95.quizgame.domain.Quiz
import java.util.*

interface QuizRepository: JpaRepository<Quiz, Long> {
    fun findByCreatorId(creatorId: Long): List<Quiz>

    fun findByUuid(uuid: UUID): Quiz?
}
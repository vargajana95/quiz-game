package vargajana95.quizgame.repository

import org.springframework.data.jpa.repository.JpaRepository
import vargajana95.quizgame.domain.Image
import java.util.*

interface ImageRepository: JpaRepository<Image, Long> {
    fun findByUuid(uuid: UUID): Image
}
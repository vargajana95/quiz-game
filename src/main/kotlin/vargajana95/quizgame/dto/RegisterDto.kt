package vargajana95.quizgame.dto

import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

class RegisterDto(
        @Email var email: String = "",
        @NotNull @NotBlank var password: String = "",
        var repeatPassword: String = "",
        var name: String = ""
)
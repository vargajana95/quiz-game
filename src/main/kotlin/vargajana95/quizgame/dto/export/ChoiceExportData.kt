package vargajana95.quizgame.dto.export

import vargajana95.quizgame.domain.Choice


class ChoiceExportData() {
    var text: String = ""
    var correct: Boolean = false

    constructor(choice: Choice):this() {
        text = choice.text
        correct = choice.correct
    }

}

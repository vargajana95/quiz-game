package vargajana95.quizgame.dto.export

import com.fasterxml.jackson.annotation.JsonIgnore
import vargajana95.quizgame.domain.Quiz

class QuizExportData() {
    var title: String = ""

    var questions: List<QuestionExportData> = listOf()
    var image: ByteArray? = null

    constructor(quiz: Quiz): this() {
        title = quiz.title
        image = quiz.image?.data
        questions = quiz.questions.map { QuestionExportData(it) }
    }
}
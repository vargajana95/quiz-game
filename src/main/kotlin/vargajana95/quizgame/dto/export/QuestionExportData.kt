package vargajana95.quizgame.dto.export

import com.fasterxml.jackson.annotation.JsonIgnore
import vargajana95.quizgame.domain.Question

class QuestionExportData() {
    var text: String = ""
    var image: ByteArray? = null
    var choices: List<ChoiceExportData> = listOf()

    constructor(question: Question): this() {
        text = question.text
        image = question.image?.data
        choices = question.choices.map { ChoiceExportData(it) }
    }

}
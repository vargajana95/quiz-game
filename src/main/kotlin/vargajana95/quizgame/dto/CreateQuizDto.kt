package vargajana95.quizgame.dto

import java.util.*
import javax.validation.Valid
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern

class CreateQuizDto {
    @Pattern(regexp="^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$", message = "Unknown id")
    @NotBlank
    var id: String = ""

    @NotBlank
    var title: String = ""

    @Valid
    var questions: List<QuestionDto> = ArrayList()

    var imageId: Long? = null

    class QuestionDto {
        @NotBlank
        var question: String = ""

        var choices: List<ChoiceDto> = ArrayList()

        var imageId: Long? = null
    }

    class ChoiceDto {
        @NotBlank
        var choice: String = ""

        var correct: Boolean = false
    }

}
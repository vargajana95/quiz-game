package vargajana95.quizgame.dto

import vargajana95.quizgame.domain.Quiz

class QuizResponseDto {

    val title: String
    val questions: List<QuestionDto>
    var imageUrl: String? = null
    var imageId: Long? = null

    constructor(title: String, questions: List<QuestionDto>) {
        this.title = title
        this.questions = questions
    }

    constructor(quiz: Quiz) {
        this.title = quiz.title
        this.imageId = quiz.image?.id
        this.imageUrl = quiz.image?.let {"/images/${it.uuid}" }
        this.questions = quiz.questions.map { q-> QuestionDto(q.text, q.image?.let {"/images/${it.uuid}" }, q.image?.id, q.choices.map { c -> ChoiceDto(c.text, c.correct) }) }
    }


    class QuestionDto (
            val question: String,
            val imageUrl: String?,
            val imageId: Long?,
            val choices: List<ChoiceDto>
    )

    class ChoiceDto(
            val choice: String,
            val correct: Boolean
    )
}

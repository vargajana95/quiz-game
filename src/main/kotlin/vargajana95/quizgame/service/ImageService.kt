package vargajana95.quizgame.service

import org.springframework.stereotype.Service
import vargajana95.quizgame.domain.Image
import vargajana95.quizgame.repository.ImageRepository
import java.util.*

@Service
class ImageService(
        val imageRepository: ImageRepository
) {


    fun saveImage(data: ByteArray): Image {
        val image = Image(UUID.randomUUID(), data)
        return imageRepository.save(image)

    }

    fun getImage(id: String): Image {
        return imageRepository.findByUuid(UUID.fromString(id))
    }

    fun getImageById(id: Long): Image {
        return imageRepository.findById(id).get()
    }


}
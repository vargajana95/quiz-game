package vargajana95.quizgame.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream
import org.springframework.security.access.prepost.PostAuthorize
import org.springframework.stereotype.Service
import vargajana95.quizgame.domain.Quiz
import vargajana95.quizgame.dto.export.QuizExportData
import vargajana95.quizgame.repository.QuizRepository


@Service
class QuizExportService(
        private val quizService: QuizService
) {

    fun exportSingleQuiz(quizId: Long): ByteArray {
        val quizToExport = quizService.getQuizById(quizId)

        return exportQuizzes(listOf(quizToExport))
    }

    fun exportQuizzes(quizzes: List<Quiz>): ByteArray {
        val quizExportDatas = quizzes.map {
            QuizExportData(it)
        }

        val mapper = ObjectMapper()

        return mapper.writeValueAsBytes(quizExportDatas)
    }

    fun exportQuizzesForUser(userId: Long): ByteArray {
        val quizzes = quizService.getQuizzesForUser(userId)

        return exportQuizzes(quizzes)
    }
}
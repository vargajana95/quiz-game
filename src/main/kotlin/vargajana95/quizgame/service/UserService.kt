package vargajana95.quizgame.service

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import vargajana95.quizgame.dto.RegisterDto
import vargajana95.quizgame.domain.AppUserDetails
import vargajana95.quizgame.domain.User
import vargajana95.quizgame.repository.UserRepository

@Service
class UserService(
        private val userRepository: UserRepository,
        private val passwordEncoder: PasswordEncoder
) : UserDetailsService {

    override fun loadUserByUsername(username: String): UserDetails {
        val user = userRepository.findByEmail(username)
                ?: throw UsernameNotFoundException("Username $username was not found!")

        return AppUserDetails(user)
    }

    fun createUser(registration: RegisterDto) {
        val user = User(email = registration.email, passwordHash = passwordEncoder.encode(registration.password), name = registration.name)
        userRepository.save(user)
    }

    fun loadCurrentUser(): User {
        return userRepository.findByEmail(SecurityContextHolder.getContext().authentication.name)!!
    }
}
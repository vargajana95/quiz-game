package vargajana95.quizgame.service

import org.springframework.data.repository.findByIdOrNull
import org.springframework.security.access.prepost.PostAuthorize
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import vargajana95.quizgame.domain.Choice
import vargajana95.quizgame.domain.Question
import vargajana95.quizgame.domain.Quiz
import vargajana95.quizgame.dto.CreateQuizDto
import vargajana95.quizgame.exception.EntityNotFoundException
import vargajana95.quizgame.repository.ChoiceRepository
import vargajana95.quizgame.repository.QuestionRepository
import vargajana95.quizgame.repository.QuizRepository
import java.time.LocalDateTime
import java.util.*

@Service
@Transactional(readOnly = true)
class QuizService(
        private val quizRepository: QuizRepository,
        private val questionRepository: QuestionRepository,
        private val choiceRepository: ChoiceRepository,
        private val userService: UserService,
        private val imageService: ImageService
) {

    fun getQuizzesForUser(userId: Long): List<Quiz> {
        return quizRepository.findByCreatorId(userId)
    }


    @PostAuthorize("returnObject.creator.id == authentication.principal.id")
    fun getQuizById(id: Long): Quiz {
        return quizRepository.findByIdOrNull(id) ?: throw EntityNotFoundException("Quiz not found!")
    }

    @Transactional
    fun createOrUpdateQuiz(quiz: CreateQuizDto) {
        val uuid = UUID.fromString(quiz.id)
        var savedQuiz = quizRepository.findByUuid(uuid)
        if (savedQuiz == null) {
            savedQuiz = Quiz(uuid, quiz.title, userService.loadCurrentUser())
            quizRepository.save(savedQuiz)
        } else {
            savedQuiz.questions.forEach{choiceRepository.deleteAllInBatchByQuestion(it)}
            questionRepository.deleteAllInBatchByQuiz(savedQuiz)
        }

        val quizImage = quiz.imageId?.let {id -> imageService.getImageById(id) }
        savedQuiz.image = quizImage
        savedQuiz.title = quiz.title
        savedQuiz.lastModified = LocalDateTime.now()
        quiz.questions.forEach {
            val questionImage = it.imageId?.let {id -> imageService.getImageById(id) }
            val question = Question(text = it.question, quiz = savedQuiz, image = questionImage)
            it.choices.forEach{
                choiceDto -> choiceRepository.save(Choice(text = choiceDto.choice, correct = choiceDto.correct, question = question))
            }
            questionRepository.save(question)
        }
    }

    fun createNewQuizForCurrentUser(): Quiz {
        return Quiz(UUID.randomUUID(), "", userService.loadCurrentUser())
    }

    @Transactional
    fun deleteQuiz(quizId: Long) {
        quizRepository.deleteById(quizId)
    }

    @Transactional
    fun saveQuiz(quiz: Quiz) {
        quizRepository.save(quiz)
        quiz.questions.forEach {
            questionRepository.save(it)
            it.choices.forEach {
                c -> choiceRepository.save(c)
            }
        }
    }
}
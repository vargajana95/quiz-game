package vargajana95.quizgame.service

import org.springframework.stereotype.Service
import vargajana95.quizgame.domain.Choice
import vargajana95.quizgame.domain.Image
import vargajana95.quizgame.domain.Question
import vargajana95.quizgame.domain.Quiz
import vargajana95.quizgame.dto.export.QuizExportData
import java.util.*

@Service
class QuizImportService(
        private val quizService: QuizService,
        private val imageService: ImageService
) {
    fun importQuizzes(quizExportDatas: List<QuizExportData>) {
        quizExportDatas.forEach {quizExportData ->
            val quiz = quizService.createNewQuizForCurrentUser()

            quiz.title = quizExportData.title
            val quizImage = quizExportData.image?.let { i -> imageService.saveImage(i) }
            quiz.image = quizImage

            quiz.questions = quizExportData.questions.map {
                val image = it.image?.let { i -> imageService.saveImage(i) }
                val question = Question(it.text, quiz, image)
                question.choices = it.choices.map { c -> Choice(c.text, c.correct, question) }.toMutableList()
                question
            }.toMutableList()

            quizService.saveQuiz(quiz)
        }
    }
}
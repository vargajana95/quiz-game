package vargajana95.quizgame.domain

import java.util.*
import javax.persistence.*

@Entity
class Image(
        val uuid: UUID,
        @Lob val data: ByteArray
        ) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null
}
package vargajana95.quizgame.domain

import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "TBL_USER")
class User(
        var email: String ="",
        var passwordHash: String="",
        var name: String = ""
): Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

}
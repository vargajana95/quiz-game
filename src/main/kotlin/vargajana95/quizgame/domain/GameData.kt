package vargajana95.quizgame.domain

import vargajana95.quizgame.dto.QuizResponseDto

class GameData(
        val gameId: String,
        val creator: String,
        val questionCount: Int,
        val quiz: QuizResponseDto
) {
    val players: MutableMap<String, Player> = hashMapOf()
    var questionNumber: Int = -1

    class Player(val name: String, val sessionId: String, questionsCount: Int) {
        val choices: MutableList<Choice> = arrayListOf()
        var currentScore: Int = 0

        init {
            for (i in 0 until questionsCount) {
                choices.add(Choice())
            }
        }
        fun getChoice(questionIndex: Int): Choice {
            return choices[questionIndex]
        }

        fun setChoice(questionIndex:Int, choice: Int?, correct: Boolean) {
            choices[questionIndex] = Choice(choice, correct)
            if (correct) {
                currentScore++
            }
        }
    }

    class Choice(
            val choice: Int? = null,
            val correct: Boolean = false
    )

    fun createPlayer(sessionId: String, name: String) {
        players.putIfAbsent(sessionId, Player(name, sessionId, questionCount))
    }

}

package vargajana95.quizgame.domain

import javax.persistence.*

@Entity
class Question(
    var text: String,
    @ManyToOne var quiz: Quiz,
    @OneToOne var image: Image? = null
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null


    @OneToMany(mappedBy = "question", cascade = [CascadeType.REMOVE])
    var choices: MutableList<Choice> = arrayListOf()


}
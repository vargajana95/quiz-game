package vargajana95.quizgame.domain

import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import org.springframework.format.annotation.DateTimeFormat
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*
import kotlin.collections.ArrayList

@Entity
class Quiz(
        @Column(unique = true, nullable = false) var uuid: UUID,
        var title: String,
        @ManyToOne var creator: User
) {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @OneToMany(mappedBy = "quiz", cascade = [CascadeType.REMOVE])
    var questions: MutableList<Question> = arrayListOf()

    @OneToOne
    var image: Image? = null

    override fun toString(): String {
        return "Quiz(uuid=$uuid, title='$title', creator=$creator, id=$id)"
    }

    @DateTimeFormat(pattern = "yyyy/MMMM/dd HH:mm")
    var lastModified: LocalDateTime = LocalDateTime.now()



}
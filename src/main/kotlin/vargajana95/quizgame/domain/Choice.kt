package vargajana95.quizgame.domain

import javax.persistence.*

@Entity
class Choice(
        var text: String,
        var correct: Boolean,
        @ManyToOne var question: Question
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

}
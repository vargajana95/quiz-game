package vargajana95.quizgame.domain

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

class AppUserDetails(email: String, passwordHash: String, name: String) : UserDetails, User(email, passwordHash, name)  {

    constructor(user: User): this(user.email, user.passwordHash, user.name) {
        id = user.id
    }


    override fun getAuthorities(): MutableCollection<out GrantedAuthority> = AuthorityUtils.createAuthorityList("ROLE_USER")


    override fun isEnabled() = true

    override fun getUsername() = email

    override fun isCredentialsNonExpired() = true

    override fun getPassword() = passwordHash

    override fun isAccountNonExpired() = true

    override fun isAccountNonLocked() = true
}
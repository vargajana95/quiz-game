package vargajana95.quizgame

import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Profile
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import vargajana95.quizgame.domain.Choice
import vargajana95.quizgame.domain.Question
import vargajana95.quizgame.domain.Quiz
import vargajana95.quizgame.domain.User
import vargajana95.quizgame.repository.ChoiceRepository
import vargajana95.quizgame.repository.QuestionRepository
import vargajana95.quizgame.repository.QuizRepository
import vargajana95.quizgame.repository.UserRepository
import java.util.*

@Component
@Profile(value = ["dev"])
class DatabaseInitializer(
        private val userRepository: UserRepository,
        private val passwordEncoder: PasswordEncoder,
        private val quizRepository: QuizRepository,
        private val questionRepository: QuestionRepository,
        private val choiceRepository: ChoiceRepository
) : CommandLineRunner {

    @Transactional
    override fun run(vararg args: String?) {
        val user1 = User(email = "wincheszter@gmail.com", name = "Winch Eszter", passwordHash = passwordEncoder.encode("password"))
        val user2= User(email = "test@test.com", name = "Test user", passwordHash = passwordEncoder.encode("password"))
        userRepository.save(user1)
        userRepository.save(user2)

        val quiz1 = Quiz(UUID.randomUUID(), "First quiz", user1)
        quizRepository.save(quiz1)
        val question1 = Question("Test question 1", quiz1)
        questionRepository.save(question1)

        val choices1 = listOf(Choice("Not correct", false, question1),
                Choice("Correct", true, question1),
                Choice("Not correct", false, question1),
                Choice("Correct", true, question1))
        choices1.forEach{choiceRepository.save(it)}

        val qquestion2 = Question("Test question 2", quiz1)
        questionRepository.save(qquestion2)

        val qchoices2 = listOf(Choice("Not correct", false, qquestion2),
                Choice("false", false, qquestion2),
                Choice("Not good", false, qquestion2),
                Choice("Correct", true, qquestion2))
        qchoices2.forEach{choiceRepository.save(it)}

        val qquestion3 = Question("Test question 3", quiz1)
        questionRepository.save(qquestion3)

        val qchoices3 = listOf(Choice("Not correct", false, qquestion3),
                Choice("Correct", true, qquestion3),
                Choice("false", false, qquestion3),
                Choice("Not good", false, qquestion3))
        qchoices3.forEach{choiceRepository.save(it)}

        val quiz2 = Quiz(UUID.randomUUID(), "Second quiz", user2)
        quizRepository.save(quiz2)
        val question2 = Question("Test question", quiz2)
        questionRepository.save(question2)

        val choices2 = listOf(Choice("Not correct", false, question2),
                Choice("Correct", true, question2),
                Choice("Not correct", false, question2),
                Choice("Correct", true, question2))
        choices2.forEach{choiceRepository.save(it)}

    }


}